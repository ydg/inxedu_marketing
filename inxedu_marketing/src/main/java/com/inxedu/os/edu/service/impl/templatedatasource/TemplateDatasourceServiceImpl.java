package com.inxedu.os.edu.service.impl.templatedatasource;

import com.inxedu.os.common.entity.PageEntity;
import com.inxedu.os.edu.dao.templatedatasource.TemplateDatasourceDao;
import com.inxedu.os.edu.entity.templatedatasource.TemplateDatasource;
import com.inxedu.os.edu.service.templatedatasource.TemplateDatasourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author www.inxedu.com
 * @description 模板动态数据来源 TemplateDatasourceService接口实现
 */
@Service("templateDatasourceService")
public class TemplateDatasourceServiceImpl implements TemplateDatasourceService {
	@Autowired
	private TemplateDatasourceDao templateDatasourceDao;
	
	/**
     * 添加模板动态数据来源
     */
    public void addTemplateDatasource(TemplateDatasource templateDatasource){
		templateDatasourceDao.addTemplateDatasource(templateDatasource);
    }
    
    /**
     * 删除模板动态数据来源
     * @param dataKey
     */
    public void delTemplateDatasourceByDataKey(Long dataKey){
    	templateDatasourceDao.delTemplateDatasourceByDataKey(dataKey);
    }
    
    /**
     * 修改模板动态数据来源
     * @param templateDatasource
     */
    public void updateTemplateDatasource(TemplateDatasource templateDatasource){
    	templateDatasourceDao.updateTemplateDatasource(templateDatasource);
    }
    
    /**
     * 通过dataKey，查询模板动态数据来源
     * @param dataKey
     * @return
     */
    public TemplateDatasource getTemplateDatasourceByDataKey(Long dataKey){
    	return templateDatasourceDao.getTemplateDatasourceByDataKey(dataKey);
    }
    
    /**
     * 分页查询模板动态数据来源列表
     * @param templateDatasource 查询条件
     * @param page 分页条件
     * @return List<TemplateDatasource>
     */
    public List<TemplateDatasource> queryTemplateDatasourceListPage(TemplateDatasource templateDatasource,PageEntity page){
    	return templateDatasourceDao.queryTemplateDatasourceListPage(templateDatasource, page);
    }
    
    /**
     * 条件查询模板动态数据来源列表
     * @param templateDatasource 查询条件
     * @return List<TemplateDatasource>
     */
    public List<TemplateDatasource> queryTemplateDatasourceList(TemplateDatasource templateDatasource){
    	return templateDatasourceDao.queryTemplateDatasourceList(templateDatasource);
    }
}



