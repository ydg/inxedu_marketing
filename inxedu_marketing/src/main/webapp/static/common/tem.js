//选择功能模块
$(function(){
	$("#imageColor").bigColorpicker(function(custom,color){
		$("#custom").css("background",color);
		$("#imageColor").val(color);
	},"L",10);
	var bn=true;
	for(var x=0;x<$(".iTemp-lmemu input[type='checkbox']").length;x++){
		if($(".iTemp-lmemu input[type='checkbox']").eq(x).is(":checked")){
			bn=false;
			if($(".iTemp-lmemu input[type='checkbox']").eq(x).val()<6){
				$("#sp").text($(".iTemp-lmemu input[type='checkbox']").eq(x).val()).show();
				$("#sp").attr("path",$(".iTemp-lmemu input[type='checkbox']").eq(x).attr("path"))
			}else{
				$("#ins_outer_wrap").append("<li id='"+$(".iTemp-lmemu input[type='checkbox']").eq(x).val()+"' path='"+$(".iTemp-lmemu input[type='checkbox']").eq(x).attr("path")+"'>"+$(".iTemp-lmemu input[type='checkbox']").eq(x).val()+"</li>");
			}
		}
	}
	if(bn){
		/*$.ajax({
			url:"/readMessage",
			type:"post",
			dataType:"json",
			success:function(result){
				if(result.success){
					for(var i=0;i<result.entity.length;i++){
						if(i!=((result.entity).length-1)){
							if(i==0){
								var tex=(result.entity)[0];
								var s=tex.split("|");
								$("#sp").text(s[0]).show();
								$("#sp").attr("path",s[1]);
								$(".iTemp-lmemu input[type='checkbox']").eq(s[0]-1).attr("checked",true);
							}else{
								var tex=(result.entity)[i];
								var s=tex.split("|");
								$("#ins_outer_wrap").append("<li id='"+s[0]+"' path='"+s[1]+"'>"+s[0]+"</li>");
								$(".iTemp-lmemu input[type='checkbox']").eq(s[0]-1).attr("checked",true);
								
							}
							
						}else{
							var bl=true;
							for(var j=0;j<$("input[name='color']").length;j++){
								if($("input[name='color']").eq(j).val()==(result.entity)[i]){
									$("input[name='color']").eq(j).attr("checked","checked");
									bl=false;
								}
							}
							if(bl){
								$("input[name='color']").eq(3).attr('checked',true);
								$("input[name='color']").eq(3).val((result.entity)[i]);
								$("#custom").css("background",$("input[name='color']").eq(3).val());
							}
						}
						
					}
				}
			}
		});*/
	}
	$(".iTemp-lmemu input[type='checkbox']").click(function(){
		if($(this).hasClass('index_c')){
			$(".index_c").attr('checked',false);
			this.checked=true;
		}
		if(this.checked){
			if($(this).val()<6){
				$("#sp").text($(this).val()).show();
				$("#sp").attr("path",$(this).attr("path"))
			}else{
				$("#ins_outer_wrap").append("<li id='"+$(this).val()+"' path='"+$(this).attr("path")+"'>"+$(this).val()+"</li>");
			}
		}else{
			if($(this).val()==$("#sp").text()){
				$("#sp").text("");
				$("#sp").hide();
			}else{
				var i=$(this).val();
				$("#ins_outer_wrap li").remove("#"+i);
			}
		}

	});
	
	
});
//点击生成
function build(){
	var array=[];
	var arr=[];
	var i=$("#ins_outer_wrap li").eq(0);
	if(i!=null&&i.text()!=""&&i.text()<6){
		for(var j=0;j<$("#ins_outer_wrap li").length;j++){
			array[j]=$("#ins_outer_wrap li").eq(j).attr("path");
			arr[j]=$("#ins_outer_wrap li").eq(j).text();
			
		}
	}else{
		msgshow("首屏必须选并且只能在首位");
		return;
	}
	
	//获取选中的颜色
	var color=$("input[name='color']:checked").val();

	var param={titles:array,tel:0,le:arr,color:color};
	$.ajax({
		url :"/createIndexPage",
		data :param,
		type : "post",
		dataType : "json",
		success : function(result) {
			if(result.success){
				
				msgshow(result.message);
			}
		},
		error:function(result){
			if(result.success){
				msgshow("系统繁忙，请稍候再试");
			}
		}
	});
}
//点击预览
function pvw(){
	var array=[];
	var i=$("#ins_outer_wrap li").eq(0);
	if(i!=null&&i.text()!=""&&i.text()<6){
		for(var j=0;j<$("#ins_outer_wrap li").length;j++){
			array[j]=$("#ins_outer_wrap li").eq(j).attr("path");
			
		}
	}else{
		msgshow("首屏必须选并且只能在首位");
		return;
	}
	//获取选中的颜色
	var color=$("input[name='color']:checked").val();
	var param={titles:array,tel:1,color:color};
	$.ajax({
		url :"/createIndexPage",
		data :param,
		type : "post",
		dataType : "json",
		success : function(result) {
			if(result.success){
				
				window.location="/indexPage	1.html";
			}
		},
		error:function(result){
			if(result.success){
				msgshow("系统繁忙，请稍候再试");
			}
		}
	});
}


