<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/base.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>模板管理</title>
<script type="text/javascript">

</script>
</head>
<body>
	<div class="">
		<table cellspacing="0" cellpadding="0" border="0" class="fullwidth">
			<caption>
				<div class="mt10 clearfix">
					<p class="fl c_666">
						<input type="button" value="上传" class="button">
						<input type="button" value="返回" class="button">
					</p>
				</div>
			</caption>
			<thead>
				<tr>
					<td align="center">名称</td>
					<td align="center">路径</td>
					<td align="center" width="120">操作</td>
				</tr>
			</thead>

			<tbody>
				<c:forEach items="${fileList}" var="tc" varStatus="index">
					<tr <c:if test="${index.count%2==1 }">class="odd"</c:if>>
						<td align="center">${tc.name}</td>
						<td align="center">${tc.path}</td>
						<td align="center">
							<button onclick="deleteTeacher(${tc.id})" class="ui-state-default ui-corner-all" type="button">删除</button>
							<button onclick="window.location.href='${ctx}/admin/teacher/toUpdate/${tc.id}'" class="ui-state-default ui-corner-all" type="button">进入</button>
							<button onclick="window.location.href='${ctx}/admin/teacher/toUpdate/${tc.id}'" class="ui-state-default ui-corner-all" type="button">修改</button>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</body>
</html>