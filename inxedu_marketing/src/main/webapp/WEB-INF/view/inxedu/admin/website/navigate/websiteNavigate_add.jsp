<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/base.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title></title>
<script type="text/javascript" src="${ctximg}/kindeditor/kindeditor-all.js?v=${v}"></script>
<link rel="stylesheet" type="text/css" href="${ctximg}/kindeditor/themes/default/default.css?v=${v}" />

<script type="text/javascript">
	function addSubmit(){
		if($("#navigateName").val()==null||$("#navigateName").val()==""){
			msgshow("名称不能为空!",'false');
			return false;
		}
		if($("#navigateUrl").val()==null||$("#navigateUrl").val()==""){
			msgshow("跳转链接不能为空!",'false');
			return false;
		}
		if($("#navigateUrl").val().charAt(0)!="/"){
			msgshow("请按正确格式填写链接!",'false');
			return false;
		}
		if (!isEmpty($("#navigateUrl").val())&&$("#navigateUrl").val().substring($("#navigateUrl").val().lastIndexOf("."))!=".html"){
			msgshow("链接必须以html结尾!",'false');
			return;
		}
		if(isNaN($("#orderNum").val())){
			msgshow("排序只能为数字",'false');
			return false;
		}
		$.ajax({
			type:"POST",
			dataType:"json",
			url:"/admin/website/addNavigate",
			data:$("#addNavigateForm").serialize(),
			success:function(result){
				if (result.success){
					window.location.href = "${ctx}/admin/website/navigates";
				}else {
					msgshow(result.message)
				}
			}
		});
	}
</script>
</head>
<body>
<div class="rMain">
	<fieldset>
		<%--<legend>
			<span>导航管理</span>
			&gt;
			<span>导航添加</span>
		</legend>--%>
		<!-- /tab4 begin -->
		<div class="mt20">
			<form action="${ctx}/admin/website/addNavigate" method="post" id="addNavigateForm">
				<span>
					导航基本属性
					<tt class="c_666 ml20 fsize12">
						（<font color="red">*</font>&nbsp;为必填项）
					</tt>
				</span>
				<p>
					<label for="sf"><font color="red">*</font>&nbsp;导航名称</label>
					<input type="text" name="websiteNavigate.name" id="navigateName" class="sf" />
					<span class="field_desc"></span>
				</p>
				<p>
					<label for="sf"><font color="red">*</font>&nbsp;跳转链接</label>
					<input type="text" name="websiteNavigate.url" id="navigateUrl" class="sf" />
					<span class="field_desc">&nbsp&nbsp&nbsp链接格式如：/hello.html</span>
				</p>
				<p>
					<label for="dropdown"><font color="red">*</font>&nbsp;在新页面打开</label> <select class="dropdown" name="websiteNavigate.newPage">
						<option value="1">否</option>
						<option value="0">是</option>
					</select>
				</p>
				<p>
					<label for="dropdown"><font color="red">*</font>&nbsp;类型</label> <select class="dropdown" name="websiteNavigate.type">
						<option value="INDEX">首页</option>
						<option value="USER">个人中心</option>
						<option value="FRIENDLINK">尾部友链</option>
						<option value="TAB">尾部标签</option>
					</select>
				</p>
				<p>
					<label for="sf"><font color="red">*</font>&nbsp;排序（由大到小显示）</label>
					<input type="text" name="websiteNavigate.orderNum" value="0" id="orderNum" class="sf" />
					<span class="field_desc"></span>
				</p>
				<p>
					<input type="button" value="提 交" class="button" onclick="addSubmit()" />
					<input type="reset" value="返 回" class="button" onclick="history.go(-1);" />
				</p>
			</form>
		</div>
</fieldset>
</div>
</body>
</html>